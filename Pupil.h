/*
 * Pupil.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef PUPIL_H_
#define PUPIL_H_
#include "Person.h"
#include <cstring>
using namespace std;

class Pupil: public Person
{
protected:
	vector <int> grades; // pupil's grades vector
	char layer; //pupil's layer
	int clNum; // pupil's class num
public:
	Pupil();// constructor for new pupil
	double getAvg();// returns pupil's average
	bool isExcellent();// returns if a specific person is outstanding (by it's type criteria)
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	bool operator >(Person& pr);// returns if a pupil's average value is bigger than other pupil's average value.
	friend istream& operator >> (istream& input, Pupil* pu);// implementation of operator ">>" for pupil
	void printDet();// printing of pupil's full details - full name , class, grades, excellency , etc.
	char getLayer(); // returns pupil layer
	int getClNum();// returns pupil's class number
	~Pupil();// pupil's destructor
};

#endif /* PUPIL_H_ */
