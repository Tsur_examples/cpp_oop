/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Secretary.h"

double Secretary::calcSalary() // returns secretary's salary.
{
	return Worker::basis+KidsInSchool*200;
}
bool Secretary::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	return (this->seniority > 10);
}
double Secretary::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return calcSalary();
}
bool Secretary::operator >(Person& pr) // returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
{
	return getVal()>pr.getVal();
}
void Secretary::printDet()// printing of secretary's full details - full name , number of kids in school ,seniority, salary, excellency , office location.
{
	cout << "the secretary-staff member's name: " << this->getName()<< endl;
	cout<< "secretary's ID: " << this->getID() << endl;
	cout << "number of Kids In School: " << this->KidsInSchool << endl;
	cout << "member's seniority: " << this->manSen << endl;
	cout << "member's salary: " << this->calcSalary()<<endl;
	cout << "member's office location: " << this->officeLoc << endl;
}
istream& operator >> (istream& input, Secretary* sec)// implementation of operator ">>" for secretary
{
	string id,loc;
	int senior,kids;
	cout << "Insert secretary's last name: "<<endl;
	while(cin.get()!='\n');
	getline(cin,sec->lName);
	cout << "Insert secretary's first name: "<<endl;
	getline(cin,sec->pName);
	cout<<"Enter secretary's Id"<<endl;
	input>>id;
	sec->id=id;
	cout<<"Enter the secretary number of children in the school"<<endl;
	do{
		cin>>kids;
		if(kids<0 )
			cout<<"please enter a positive number of kids"<<endl;
	}while(kids<0);
	sec->KidsInSchool=kids;
	do{
		cout<<"Please enter the managing seniority"<<endl;
		input>>senior;
		if(senior<0)
		{
			cout<<"Managing seniority can't be less than 0"<<endl;
		}
	}while(senior<0);
	sec->manSen=senior;
	cout<<"Please enter the office location"<<endl;
	while(cin.get()!='\n');
	getline(cin,loc);
	sec->officeLoc=loc;
	return input;
}
Secretary::~Secretary() //secretary's destructor
{

}
Secretary::Secretary() //empty constructor
:AdministrationPersonal()
{
	KidsInSchool=0;
}
