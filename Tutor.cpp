/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Tutor.h"

double Tutor::calcSalary()  // returns tutor's salary.
{
	return this->Teacher::calcSalary()+1000;
}
double Tutor::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return calcSalary();
}
bool Tutor::operator >(Person& pr) // returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
{
	return getVal()>pr.getVal();
}
bool Tutor::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	int excelCount;
	for (int i=0 ; i<myClass->getPupilsNum() ; i++)
	{
		if(myClass->getPupilAt(i)->isExcellent())
		{
			excelCount++;
		}
	}
	if (myClass->getPupilsNum()!=0 && excelCount/myClass->getPupilsNum()>0.5)
		return true;
	return false;
}
istream& operator >> (istream& input, Tutor* tut) // implementation of operator ">>" for tutor
{
	Class* cl;
	bool flag=true;
	string id,prof;
	char layer;
	int classNum,senior;
	cout << "Insert Tutor's last name: "<<endl;
	while(cin.get()!='\n');
	getline(cin,tut->lName);
	cout << "Insert Tutor's first name: "<<endl;
	getline(cin,tut->pName);
	cout<<"Enter Tutor's Id"<<endl;
	input>>id;
	tut->id=id;
	cout<<"Please enter the professions (-1 to stop)"<<endl;
	cout << "enter in this format: profession then enter, profession then enter.."<<endl;
	while(cin.get()!='\n');
	do{
		flag=true;
		getline(input,prof);
		if(prof!="-1")
		{
			for(int i=0;i<tut->profsNum;i++)
			{
				if(tut->professions[i]==prof)
				{
					cout<<"this tutor has this profession already"<<endl;
					flag = false;
					break;
				}
			}
			if (flag)
			{
				tut->professions.push_back(prof);
				tut->profsNum++;
			}
		}
		if(tut->profsNum==0)
			cout<<"Teacher has to teach at least one subject"<<endl;
	}while(prof!="-1" || tut->profsNum==0);
	cout<<"Please enter the teaching seniority"<<endl;
	do{
		input>>senior;
		if(senior<0)
		{
			cout<<"teaching seniority can't be less than 0!!!"<<endl;
		}
	}while(senior<0);
	tut->seniority=senior;
	do{
		cout << "Insert tutor's layer ('a'-'f'): "<<endl;
		while(cin.get()!='\n');
		input>>layer;
		if(!(layer>='a' && layer <='f'))
			cout<<"Wrong layer."<<endl;

	}while(!(layer>='a' && layer <='f'));
	do{
		cout << "Insert tutor's class number (1-3): "<<endl;
		while(cin.get()!='\n');
		input>>classNum;

		if(!(classNum>=0 && classNum<=3))
			cout<<"Wrong class number."<<endl;

	}while(!(classNum>=0 && classNum<=3));
	cl= new Class(layer,classNum,tut);
	tut->myClass=cl;
	return input;

}
void Tutor::printDet() // printing of tutor's full details - full name , seniority, salary, excellency , tutor's class.
{
	cout<< "tutor's name: ";
	cout << this->getName() << endl;
	cout<< "tutor's ID: " << this->getID() << endl;
	cout << "tutor's seniority: "<< seniority << endl;
	cout<< "tutor's salary: "<< this->calcSalary()<<endl;
	this->Teacher::printProfs();
	if (this->Teacher::isExcellent())
	{
		cout << "this Teacher is an excellent one!"<< endl;
	}
	cout << "the tutor's class: "<<endl;
	for (int i= 0 ; i < myClass->getPupilsNum() ; i++)
	{
		cout<<endl;
		myClass->getPupilAt(i)->printDet();
	}
}
Tutor::~Tutor() // destructor for tutor
{
	delete myClass;
}
Tutor::Tutor() //empty constructor for new tutor
:Teacher()
{
	myClass=NULL;
}
Class* Tutor::getClass() // returns pointer to tutor's class
{
	return myClass;
}
