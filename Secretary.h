/*
 * Secretary.h
 *
 *  Created on: Jun 19, 2016
 *      Author: victorr
 */

#ifndef SECRETARY_H_
#define SECRETARY_H_

#include "AdministrationPersonal.h"

class Secretary: public AdministrationPersonal
{
protected:
	int KidsInSchool; // secretary's number of kids in school
public:
	Secretary();//empty constructor
	int getKidsNum(){return KidsInSchool;}; //returns secretary's number of kids in school
	double calcSalary();// returns secretary's salary.
	bool isExcellent();// returns if a specific person is outstanding (by it's type criteria)
	void printDet();// printing of secretary's full details - full name , number of kids in school ,seniority, salary, excellency , office location.
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	bool operator >(Person& pr);// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	friend istream& operator >> (istream& input, Secretary* sec);// implementation of operator ">>" for secretary
	~Secretary();//secretary's destructor
};

#endif /* SECRETARY_H_ */
