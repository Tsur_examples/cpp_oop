#ifndef WORKER_H_
#define WORKER_H_

#include "Person.h"

class Worker: public Person {
protected:
	int seniority; // worker's teaching seniority
	int manSen; // worker's managing seniority
	static double basis; // A worker's base salary.
public:
	void printDet();// printing of worker's full details - full name , seniorities ,excellency, etc.
	Worker();// empty constructor for new worker.
	virtual double calcSalary();// returns worker's salary.
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	bool operator >(Person& pr);// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	~Worker();// destructor for a worker.
};

#endif /* WORKER_H_ */
