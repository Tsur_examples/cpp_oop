/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Manager.h"
Manager* Manager::theMan=0; // static pointer to manager. NULL value at beginning.

Manager* Manager::Instance() // a function that makes sure that the user cannot create more than one manager
{
	if(!theMan) // if there is no instance of manager yet - create new one.
	{
		bool flag=true;
		theMan = new Manager();
		string pname,lname,id,prof,loc;
		int senior,mansen;
		cout << "Insert manager's last name: "<<endl;
		while(cin.get()!='\n');
		getline(cin,theMan->lName);
		cout << "Insert manager's first name: "<<endl;
		getline(cin,theMan->pName);
		cout<<"Enter manager's Id"<<endl;
		cin>>theMan->id;
		cout<<"Please enter the professions (-1 to stop)"<<endl;
		cout << "enter in this format: profession then enter, profession then enter.."<<endl;
		while(cin.get()!='\n');
		do{
			flag=true;
			getline(cin,prof); //
			if(prof!="-1")
			{
				for(int i=0;i<theMan->profsNum;i++)
				{
					if(theMan->professions[i]==prof)
					{
						cout<<"this manager has this profession already"<<endl;
						flag = false;
						break;
					}
				}
				if (flag)
				{
					theMan->professions.push_back(prof);
					theMan->profsNum++;
				}
			}
		}while(prof!="-1");
		if(theMan->profsNum>0) //only if he is teaching
		{
			cout<<"Please enter the teaching seniority"<<endl;
			do{
				cin>>senior;
				if(senior<0)
				{
					cout<<"teaching seniority can't be less than 0!!!"<<endl;
				}
			}while(senior<0);
			theMan->seniority=senior;
		}
		cout<<"Please enter the managing seniority"<<endl;
		do{
			cin>>mansen;
			if(mansen<0)
			{
				cout<<"managing seniority can't be less than 0"<<endl;
			}
		}while(mansen<0);
		theMan->manSen=mansen;
		cout<<"Please enter the office location"<<endl;
		while(cin.get()!='\n');
		getline(cin,loc);
		theMan->officeLoc=loc;
		return theMan;
	}
	else // instance of manager already exists. cannot create more.
		cout<<"cannot make more than 1 instance of manager!"<<endl;
	return theMan;
}
double Manager::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return calcSalary();
}
double Manager::calcSalary()  // returns manager's salary.
{
	double salary=0;
	if(this->profsNum>0)
	{
		salary += Teacher::calcSalary();
	}
	salary+= AdministrationPersonal::basis*2+AdministrationPersonal::manSen*500;
	return salary;
}
bool Manager::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	if(this->AdministrationPersonal::manSen>3)
		return true;
	return false;
}

void Manager::printDet() // printing of tutor's full details - full name , ID, managing seniority,professions, salary, excellency.
{
	cout<< "Manager's name: ";
	cout << this->getName() << endl;
	cout<< "Manager's ID: ";
	cout << this->getID() << endl;
	cout << "Manager's seniority: "<< AdministrationPersonal::manSen << endl;
	cout << "Manager's office location: " << this->officeLoc << endl;
	cout<< "Manager's salary: "<<this->calcSalary()<<endl;
	if ((int)professions.size()>0)
	{
		cout << "Manager's professions: "<<endl;
		for (int i =0; i< (int)professions.size();i++)
		{
			cout << professions[i]<<endl;
		}
	}
	else cout<<"Manager is not teaching!"<<endl;
	if (this->isExcellent())
	{
		cout << "This manager is an excellent one!"<< endl;
	}
}
Manager::~Manager()//destructor for manager
{
	delete theMan;
}
bool Manager::operator >(Person* pr) // returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
{
	return getVal()>pr->getVal();
}
Manager::Manager() //empty constructor for new manager
:AdministrationPersonal(),Teacher()
{
}
