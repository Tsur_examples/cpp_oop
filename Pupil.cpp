/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Pupil.h"

double Pupil::getAvg() // returns pupil's average
{
	double sum=0;
	if ((int)grades.size()>0)
	{
		for (int i =0;i< (int)grades.size();i++)
			sum+=grades[i];
		return sum/(int)grades.size();
	}
	else return 0;
}
double Pupil::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return getAvg();
}
bool Pupil::operator > (Person& pr) // returns if a pupil's average value is bigger than other pupil's average value.
{
	return getVal()>pr.getVal();
}
bool Pupil::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	if ((int)grades.size()>0)
	{
		for (int i =0;i< (int)grades.size();i++)
			if(grades[i]<65)
				return false;
	}
	if (this->getAvg()>85)
		return true;
	return false;
}
void Pupil::printDet() // printing of pupil's full details - full name , class, grades, excellency , etc.
{
	cout<< "Pupil's name: ";
	cout << this->getName() << endl;
	cout<< "Pupil's ID: ";
	cout << this->getID() << endl;
	cout << "Pupil's class: "<< layer << " "<<clNum<<endl;
	if ((int)grades.size()>0)
	{
		cout << "Pupil's grades: "<<endl;
		for (int i =0; i< (int)grades.size();i++)
		{
			cout << grades[i]<<",";
		}
		cout<<"\nPupil's average:"<<getAvg()<<endl;
	}
	else cout<< "Pupil has no grades yet."<<endl;
	if (this->isExcellent())
	{
		cout << "this pupil is an excellent one!"<< endl;
	}
}
istream& operator >> (istream& input, Pupil* pu) // implementation of operator ">>" for pupil
{
	string id;
	char layer;
	int classNum,grade,grCount=1;
	cout << "Insert pupil's last name: "<<endl;
	while(cin.get()!='\n');
	getline(cin,pu->lName);
	cout << "Insert pupil's first name: "<<endl;
	getline(cin,pu->pName);
	cout<<"Enter pupil's Id"<<endl;
	input>>id;
	pu->id=id;
	do{
		cout << "Insert pupil's layer ('a'-'f'): "<<endl;
		while(cin.get()!='\n');
		input>>layer;
		if(!(layer>='a' && layer <='f'))
			cout<<"Wrong layer."<<endl;
	}while(layer<'a' || layer >'f');
	pu->layer=layer;
	do{
		cout << "Insert pupil's class number (1-3): "<<endl;
		while(cin.get()!='\n');
		input>>classNum;
		if(classNum < 0 || classNum > 3)
			cout<<"Wrong class number."<<endl;
	}while(classNum < 0 || classNum > 3);
	pu->clNum=classNum;
	cout<<"Please enter the grades (-1 to stop)"<<endl;
	do {
		while(cin.get()!='\n');
		cout<<"grade " << grCount<<": ";
		input>>grade;
		if (grade!=-1 && (grade > 100 || grade < 0))
			cout<< "invalid grade. please enter grade in the range 0 - 100."<<endl;
		else if( grade!=-1)
		{
			grCount++;
			pu->grades.push_back(grade);
		}
	} while (grade!=-1);
	return input;
}
Pupil::Pupil() // constructor for new pupil
: Person()
{
	layer ='0';
	clNum=0;
}
Pupil::~Pupil() // pupil's destructor
{
}
char Pupil::getLayer() // returns pupil layer
{
	return layer;
}
int Pupil::getClNum() // returns pupil's class number
{
	return clNum;
}
