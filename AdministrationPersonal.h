/*
 * AdministrationPersonal.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef ADMINISTRATIONPERSONAL_H_
#define ADMINISTRATIONPERSONAL_H_

#include "Worker.h"

class AdministrationPersonal:virtual public Worker {
protected:
string officeLoc; // Administration Personal's office location
public:
    AdministrationPersonal();//Administration Personal's empty constructor
	virtual double calcSalary()=0; //returns Administration Personal's salary
	virtual bool isExcellent()=0; //reutuns if an Administration Personal is outstanding
	virtual void printDet()=0; // printing Administration Personal's details
	~AdministrationPersonal();//Administration Personal's destructor
};

#endif /* ADMINISTRATIONPERSONAL_H_ */
