/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Layer.h"

Layer::Layer(char layerName) { // layer's constructor
	this->layerName=layerName;
}
Class* Layer::getClassAt(int i) // returns class at given index
{
	if((int)classes.size()>0 && i>=0 && i<=2)
		return classes[i];
	else
	{
		cout << "no classes in this layer / wrong class index." << endl;
		return 0;
	}
	return 0;
}
void Layer::addClass(Class * cl) // adds class to the layer
{
	for (int i =0 ; i < (int)classes.size(); i++)
		if (classes[i]->getClNum() == cl->getClNum())
		{
			return;
		}
	classes.push_back(cl);
}
char Layer::getLName() // returns layer name ('a'-'f')
{
	return layerName;
}
Layer::~Layer() //destructor for layer
{

}

