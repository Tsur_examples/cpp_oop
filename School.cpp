/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "School.h"

School* School::school=0; // static pointer to school. NULL value at beginning.

School::School() // empty constructor for school (private)
{
	this->layerCount=0;
	arr = new MyArray <Person>();
}
School* School::Instance() // a function that makes sure that the user cannot create more than one school
{
	if(!school) // if there is no instance of school yet
		return school = new School();
	else                // instance of school already exists. cannot create more.
		cout<<"cannot make more than 1 instance of school!"<<endl;
	return school;
}
void School::printmenu() // prints the menu for the user to use.
{
	cout << "1.  Add pupil"<<endl;
	cout << "2.  Add teacher"<<endl;
	cout << "3.  Add Tutor"<<endl;
	cout << "4.  Add manager"<<endl;
	cout << "5.  Add Secretary"<<endl;
	cout << "6.  Print person detail"<<endl;
	cout << "7.  Print outstanding people"<<endl;
	cout << "8.  Print tutor’s class"<<endl;
	cout << "9.  Highest paid worker"<<endl;
	cout << "10. Exit"<<endl;
}
void School::menu() // the menu that executes the actions offered in the school system
{
	int num;
	do {
		printmenu(); //prints out menu choices
		cin >> num;
		switch (num)
		{
		case 1:AddPupil();break; // adds new pupil to school
		case 2:AddTeacher();break; // adds new teacher to school
		case 3:AddTutor();break; // adds new tutor to school
		case 4:AddManager();break; // adds new manager to school
		case 5:AddSecretary();break; // adds new secretary to school
		case 6:PrintPersonDet();break; // prints out details of all people in the school
		case 7:PrintExcellents();break; // prints out outstanding people in the school, each one by it's on criteria
		case 8:TutorsClass();break; // prints out a specific tutuor's class
		case 9:HighestPaid();break; // prints out highest paid people in the school
		case 10:break; // exits menu and terminates program
		}
	}while (num!=10);
	delete this;
}

void School::AddPupil() // adds new pupil to school
{
	Pupil* newPu=new Pupil();
	cin>>newPu; // implementation of operator ">>" for pupil
	for(int i=0;i<arr->elCount;i++)
	{
		if(typeid (arr->getElements()[i])==typeid(Tutor*))
		{
			if(dynamic_cast<Tutor*> (arr->getElements()[i])->getClass()->isEqual(newPu->getLayer(),newPu->getClNum())) //if there exists a class, same as new pupil's class
			{
				dynamic_cast<Tutor*> (arr->getElements()[i])->getClass()->addPupil(newPu); // add new pupil to it's tutor's class
			}
		}
	}
	arr->addToArr(newPu); // add pupil to array of school people
}
void School::AddTeacher() // adds new teacher to school
{
	Teacher* newTeach=new Teacher();
	cin>>newTeach; // implementation of operator ">>" for teacher
	arr->addToArr(newTeach); // add teacher to array of school people
}
void School::AddTutor() // adds new tutor to school
{
	bool flag=true;
	Tutor* newTut=new Tutor();
	cin>>newTut; // implementation of operator ">>" for tutor
	for(int i=0;i<arr->elCount;i++) // search for tutor's pupils
	{
		if((dynamic_cast<Pupil*>(arr->getElements()[i])!=NULL))
		{
			if(newTut->getClass()->isEqual(dynamic_cast<Pupil*>(arr->getElements()[i])->getLayer(),dynamic_cast<Pupil*>(arr->getElements()[i])->getClNum())) // if there exist pupils with same class as new tutor's class
			{
				newTut->getClass()->addPupil(dynamic_cast<Pupil*>(arr->getElements()[i])); // add pupils to tutor's class
			}
		}
	}
	for(int i=0;i<arr->elCount;i++) // searching for a tutor with same class as new tutor
	{
		if((dynamic_cast<Tutor*>(arr->getElements()[i])!=NULL))
		{
			if(newTut->getClass()->isEqual(dynamic_cast<Tutor*>(arr->getElements()[i])->getClass()->getLayer(),dynamic_cast<Tutor*>(arr->getElements()[i])->getClass()->getClNum())) // tutor's class same as other tutor's class. impossible.
			{
				cout << "there can't be more than one tutor for a specific class!!" <<endl;
				return; // no adding of nwe tutor to school people
			}
		}
	}
	arr->addToArr(newTut); // add tutor to array of school people
	if (layerCount==0) // no layers created yet , create and add new layer to school's layers
	{
		Layer* Lay = new Layer (newTut->getClass()->getLayer());
		Lay->addClass(newTut->getClass());
		layers.push_back(Lay);
		return;
	}
	for (int k=0 ; k< layerCount ; k++) // searching if same layer exists. if yes - no new layer needs to be added to school
	{
		if(layers[k]->getLName() == newTut->getClass()->getLayer())
		{
			flag = false;
		}
	}
	if (flag && layerCount < 6) // no matching layer found. add layer to school
	{
		Layer* Lay = new Layer (newTut->getClass()->getLayer());
		Lay->addClass(newTut->getClass());
		layers.push_back(Lay);
	}
}
void School::AddManager() // adds new manager to school
{
	Manager *newMan=Manager::Instance(); // a function that makes sure that the user cannot create more than one manager
	for(int i=0;i<arr->elCount;i++) // searching if manager already exists in school people. if he does - no adding to school people.
	{
		if(arr->getElements()[i]==newMan)
			return;
	}
	arr->addToArr(newMan); // add manager to array of school people
}
void School::AddSecretary() // adds new secretary to school
{
	Secretary* newSec=new Secretary();
	cin>>newSec;  // implementation of operator ">>" for secretary
	if(newSec->getKidsNum() > arr->elCount) // if secretary has more kids in school than the amount of people in school
	{
		cout<<"The number of kids can't be bigger than the number of people in the school"<<endl;
		return;
	}
	arr->addToArr(newSec); // add secretary to array of school people
}
void School::PrintPersonDet() // prints out details of all people in the school
{
	arr->printall();
}
void School::PrintExcellents() // prints out outstanding people in the school, each one by it's on criteria
{
	arr->Print_outstanding_people();
}
void School::TutorsClass() // prints out a specific tutuor's class
{
	//Person* tutor;
	string ID;
	if(this->arr->elCount==0) // no people in school
	{
		cout<<"array is empty.."<<endl;
		return;
	}
	cout<<"Enter Tutor's ID"<<endl;
	cin>>ID;
	for(int i=0 ; i<this->arr->elCount;i++)
	{
		if(this->arr->getElements()[i]->getID()==ID){
			if((dynamic_cast<Tutor*>(arr->getElements()[i])!=NULL)) //relevant tutor found
			{
				arr->getElements()[i]->printDet(); // prints tutor's details and class
				return;
			}
		}
	}
	cout<<"the ID doesn't match a tutor."<<endl; //no matching tutor found
	return;
}
void School::HighestPaid() // prints out highest paid people in the school
{
	arr->printMax<Worker>();
}
School::~School() // school's destructor
{
	delete arr;
	for(int i=0;i<(int)layers.size();i++)
	{
		delete layers[i];
	}
	layers.clear();
}

