/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include <iostream>
#include "Manager.h"
#include "School.h"

using namespace std;

int main()
{
	School * sch1 = School::Instance(); // creates a new school.
	sch1->menu();
	return 0;
}

