/*
 * Person.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef PERSON_H_
#define PERSON_H_
#include <cstring>
#include <vector>
#include <iostream>
using namespace std;

class Person
{
protected:
	string pName;
	string id;
	string lName;
public:
	virtual string getID();// returns person's ID
	string getName();//returns person's full name
	virtual void printDet();// printing of person's full details.
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	virtual bool isExcellent();// returns if a specific person is outstanding (by it's type criteria)
	//bool operator >(Person& pr);
	Person();//empty constructor for new person
	virtual ~Person();// destructor for person
};

#endif /* PERSON_H_ */
