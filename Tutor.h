/*
 * Tutor.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef TUTOR_H_
#define TUTOR_H_

#include "Teacher.h"
#include "Class.h"

class Class;

class Tutor: public Teacher
{
protected:
	Class * myClass; // tutor's class
public:
	Tutor();//empty constructor for new tutor
	virtual double calcSalary();// returns tutor's salary.
	virtual bool isExcellent();// returns if a specific person is outstanding (by it's type criteria)
	bool operator >(Person& pr);// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	virtual void printDet();// printing of tutor's full details - full name , seniority, salary, excellency , tutor's class.
	friend istream& operator >> (istream& input, Tutor* tut);// implementation of operator ">>" for tutor
	~Tutor();// destructor for tutor
	Class* getClass();// returns pointer to tutor's class
};

#endif /* TUTOR_H_ */
