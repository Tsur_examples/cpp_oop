/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */
#include "Class.h"

Class::Class(char layer,int classNum,Tutor* tutor) // constructor for new class
{
this->layer=layer;
this->classNum=classNum;
this->ClPupils=0;
this->tutor =tutor;
}
Pupil* Class::getPupilAt(int i) //returns class's pupil at given index
{
	if(ClPupils>0 && i < ClPupils)
		return pupils[i];
	else
		{
		cout << "no pupils in this class" << endl;
		return 0;
		}
	return 0;
}
int Class::getPupilsNum() // returns number of pupils in class
{
	return ClPupils;
}
void Class::addPupil(Pupil* pu) // adds pupil to class
{
	pupils.push_back(pu);
	ClPupils++;
}
Class::~Class() // destructor
{
}
char Class::getLayer() //returns class's layer
{
	return layer;
}
char Class::getClNum() // returns class number
{
	return classNum;
}
bool Class::isEqual(char layer,int classNum) // returns comparison result (of layer and class number) with this class and a given layer and class number
{
	return (this->classNum==classNum && this->layer==layer);
}
