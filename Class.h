/*
 * Class.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef CLASS_H_
#define CLASS_H_

#include "Pupil.h"
#include <vector>
#include "Tutor.h"

class Tutor;

class Class {
protected:
	char layer; // class's layer
	int classNum; // class's class number
	vector <Pupil*> pupils; // class's pupils
	int ClPupils; //amount of class's pupils
	Tutor* tutor;
public:
	char getLayer();//returns class's layer
	char getClNum();// returns class number
	int getPupilsNum();// returns number of pupils in class
	Pupil* getPupilAt(int i);//returns class's pupil at given index
	void addPupil(Pupil* pu);// adds pupil to class
	Class(char layer,int classNum,Tutor* tutor);
	~Class();// destructor for class
	bool isEqual(char layer,int classNum);// returns comparison result (of layer and class number) with this class and a given layer and class number

};

#endif /* CLASS_H_ */
