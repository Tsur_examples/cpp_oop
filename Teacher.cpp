/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Teacher.h"

double Teacher::calcSalary() // returns teacher's salary.
{
	return (Worker::basis*(1+profsNum/10)+300*seniority);
}
bool Teacher::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	if (profsNum>=5)
		return true;
	return false;
}
double Teacher::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return calcSalary();
}
bool Teacher::operator >(Person& pr) // returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
{
	return getVal()>pr.getVal();
}
void Teacher::printProfs() // prints professions that the teacher teaches.
{
	if ((int)professions.size()>0)
	{
		cout << "Teacher's professions: "<<endl;
		for (int i =0; i< (int)professions.size();i++)
		{
			cout << professions[i]<<endl;
		}
	}
}
void Teacher::printDet() // printing of tutor's full details - full name , ID, seniority, salary, excellency.
{
	cout<< "Teacher's name: ";
	cout << this->getName() << endl;
	cout<< "Teacher's ID: " << this->getID() << endl;
	cout << "Teacher's seniority: "<< seniority << endl;
	cout<< "Teacher's salary: "<<calcSalary()<<endl;
	this->printProfs();
	if (this->isExcellent())
	{
		cout << "this Teacher is an excellent one!"<< endl;
	}
}
Teacher::~Teacher() // teacher's destructor
{
}
istream& operator >> (istream& input, Teacher* teach) // implementation of operator ">>" for teacher
{
	string id,prof;
	bool flag=true;
	int senior;
	cout << "Insert teacher's last name: "<<endl;
	while(cin.get()!='\n');
	getline(cin,teach->lName);
	cout << "Insert teacher's first name: "<<endl;
	getline(cin,teach->pName);
	cout<<"Enter teacher's Id"<<endl;
	input>>id;
	teach->id=id;
	cout<<"Please enter the professions (-1 to stop)"<<endl;
	cout << "enter in this format: profession then enter, profession then enter.."<<endl;
	while(cin.get()!='\n');
	do{
		flag=true;
		getline(input,prof);
		if(prof!="-1")
		{
			for(int i=0;i<teach->profsNum;i++)
			{
				if(teach->professions[i]==prof)
				{
					cout<<"this teacher has this profession already"<<endl;
					flag = false;
					break;
				}
			}
			if (flag)
			{
				teach->professions.push_back(prof);
				teach->profsNum++;
			}
		}
		if(teach->profsNum==0)
		{
			cout<<"Teacher has to teach at least one subject"<<endl;
		}
	}while(prof!="-1" ||teach-> profsNum==0);
	cout<<"Please enter the teaching seniority"<<endl;
	do{
		input>>senior;
		if(senior<0)
		{
			cout<<"teaching seniority can't be less than 0!!!"<<endl;
		}
	}while(senior<0);
	teach->seniority=senior;
	return input;
}
Teacher::Teacher() //empty constructor for new teacher
:Worker()
{
	profsNum=0;
	seniority=0;
	manSen=0;
}
vector<string>*Teacher:: getProfessions() // returns reference to teacher's professions vector
{
	return &professions;
}
