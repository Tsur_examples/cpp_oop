/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Person.h"

string Person::getName() //returns person's full name
{
	string Name="";
	Name = pName;
	Name.append(" ");
	return Name.append(lName);
}
string Person::getID() // returns person's ID
{
	return id;
}
double Person::getVal()  //a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	throw "the maximum of a Person type is not defined";
	return 0;
}
Person::~Person() // destructor for person
{
}
void Person::printDet() // printing of person's full details.
{
	cout<<"in printDet"<<endl;
}
bool Person::isExcellent() // returns if a specific person is outstanding (by it's type criteria)
{
	return false;
}
Person::Person() //empty constructor for new person
{
}
