/*
 * School.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef SCHOOL_H_
#define SCHOOL_H_
#include <cstring>
#include <vector>
#include <iostream>
#include "Layer.h"
#include "MyArray.h"
#include "Manager.h"
#include "Pupil.h"
#include "Secretary.h"
class School {
private:
	int layerCount; //number of layers the school has
	vector <Layer*> layers; //school's layers
	MyArray <Person> * arr; //school people
	static School* school; // static school pointer
	School(); // empty constructor for school (private)
	~School();// school's destructor

public:
	void AddPupil();// adds new pupil to school
	void AddTeacher();// adds new teacher to school
	void AddTutor();// adds new tutor to school
	void AddManager();// adds new manager to school
	void AddSecretary();// adds new secretary to school
	void PrintPersonDet();// prints out details of all people in the school
	void PrintExcellents();// prints out outstanding people in the school, each one by it's on criteria
	void TutorsClass();// prints out a specific tutuor's class
	void HighestPaid();// prints out highest paid people in the school
	void printmenu();// prints the menu for the user to use.
	void menu();// the menu that executes the actions offered in the school system
	static School* Instance();// a function that makes sure that the user cannot create more than one school
};

#endif /* SCHOOL_H_ */
