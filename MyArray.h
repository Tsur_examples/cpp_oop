/*
 * MyArray.h
 *
 *  Created on: Jun 19, 2016
 *      Author: victorr
 */

#ifndef MYARRAY_H_
#define MYARRAY_H_
#include <vector>
#include <iostream>
#include <typeinfo>

namespace std {

template <class T> class MyArray
{
private:
	T** elements; // array of template elements
public:
	int elCount; // amount of template elements array
	static int count; // amount of template objects created
	T& operator[](int i); // implementation of operator "[]" for pupil
	void swap(int i, int j); //swaps two elements in the array, by given swapping indices
	void printElement(int i); // prints element in index i
	void Print_outstanding_people(); //prints outstandting people
	T** getElements(){return elements;}; // returns array of elements
	template<class X>
	void printMax() // prints max element
	{
		bool flag=false;
		if(elCount==0) //no elements
		{
			cout << "array is empty.."<<endl;
		}
		else
		{
			int max=0;
			for(int i=0;i<elCount;i++) // search for max element
			{
				if((dynamic_cast<X*>(elements[i])!=NULL) && ((*elements[i]).getVal()>max))
				{
					max = (*elements[i]).getVal();
					flag = true;
				}
			}
			for(int i=0 ; i<elCount ; i++) // printing of all the max values (if there were many equal to max value)
			{
				if((dynamic_cast<X*>(elements[i])!=NULL) && ((*elements[i]).getVal()==max))
				{
					cout << (*elements[i]).getName() <<endl;
						cout << "Salary: "<< dynamic_cast<Worker*>(elements[i])->calcSalary()<<endl;
				}
			}
			if (!flag) // flag indicates if there were workers found
				cout << "no workers in the school!!"<<endl;
		};
	}
	template<class X>
	int find_total() // returns total number of template objects created
	{
		int count=0;
		for(int i=0;i<elCount;i++)
		{
			if(typeid(*elements[i])==typeid(X))
				count++;
		}
		return (count);
	};

	bool addToArr(T* toAdd); // adds template element to array
	int countT();//
	void increaseSize();// Increases array size by 1 , copies elements to increased-size array
	void printall(); // prints all elements in array
	MyArray(); //Constructor
	~MyArray(); // destructor
};

} /* namespace std */

#endif /* MYARRAY_H_ */

//begining of cpp file
namespace std {
template <class T>
int MyArray<T>::count=0;
template <class T>
MyArray<T>::MyArray() // constructor
{
	elCount=0;
	elements=NULL;
	MyArray<Person>::count++; // Increases count when any template object created
}
template <class T>
bool MyArray<T>::addToArr(T* toAdd)// adds template element to array
{
	for(int i=0;i<elCount;i++){
		if(dynamic_cast<Person*>(elements[i])->getID()==dynamic_cast<Person*>(toAdd)->getID())
		{
			cout<<"there is a person with this ID in the school already."<<endl;
			return false;
		}
	}
	increaseSize();
	elements[elCount-1]=toAdd;
	return true;
}
template <class T>
void MyArray<T>:: increaseSize() // Increases array size by 1 , copies elements to increased-size array
{
	T** temp=new T*[elCount+1];
	if(elCount!=0)
	{
		for(int j=0;j<elCount;j++)
		{
			temp[j]=elements[j];
		}
		delete [] elements;
	}
	elements=temp;
	elements[elCount]=NULL;
	elCount++;
}
template <class T>
T& MyArray<T>::operator[](int i) // implementation of operator "[]" for pupil. returns element in index i
{
	if (i>=0 && i < elCount)
		return elements[i];
	cout<<"recieved index: " <<i<<endl;
	cout<<"bounds are 0 to:" <<elCount<<endl;
	throw "index out of bounds";
}
template <class T>
void MyArray<T>::swap(int i, int j) //swaps two elements in the array, by given swapping indices
{
	T inI;
	inI = elements[i];
	if (i<0 || i> elCount || j<0 || j> elCount)
	{
		if (i<0 || i> elCount) //first index out of bounds
		{
			cout<<"recieved index: " <<i<<endl;
			cout<<"bounds are 0 to:" <<elCount<<endl;
			cout<<"first index out of bounds"<<endl;
		}
		if (j<0 || j> elCount)//second index out of bounds
		{
			cout<<"recieved index: " <<j<<endl;
			cout<<"bounds are 0 to:" <<elCount<<endl;
			throw "second index out of bounds"; //exception thrown
		}
	}
	else
	{
		elements[i]=elements[j];  //swapping
		elements[j]=inI;
	}
}
template <class T>
void MyArray<T>::Print_outstanding_people() //prints out all outStanding people by most specific type.
{
	bool flag=false;
	for(int i=0;i<elCount;i++) //search for outstanding people
	{
		if (elements[i]->isExcellent())
		{
			elements[i]->printDet();
			flag=true;
		}
	}
	if(!flag)// sad message
	{
		cout<<"no outstanding people in this school!! =[[[[[[[[[[[[[[[[[[[["<<endl;
	}
}
template <class T>
void MyArray<T>::printElement(int i) // prints element in index i
{
	if (i<0 || i> elCount)
	{
		cout<<"Received index: " <<i<<endl;
		cout<<"bounds are 0 to:" <<elCount<<endl;
		throw "index out of bounds";
	}
	else
		elements[i]->printDet();
}
template <class T>
void MyArray<T>::printall()// prints all elements in array
{
	for(int i=0;i<elCount;i++)
	{
		printElement(i);
		cout<<endl;
	}
}

template<class T> // returns number of template objects created
int MyArray<T>::countT()
{
	return MyArray<Person>::count;
}
template <class T>
MyArray<T>::~MyArray() //destructor
{
	if(elCount!=0)
	{
		for(int i=0;i<elCount;i++)
		{
			delete elements[i];
		}
		delete[] elements;
	}
}
} /* namespace std */
