/* Assignment: 6
Author1: Victor Martinov,
ID: 307835249
Author2: Tsur Avneri,
ID: 308134485
 */

#include "Worker.h"

double Worker::basis=25; // A worker's base salary.

void Worker::printDet() // printing of worker's full details - full name , seniorities ,excellency, etc.
{
	cout<< "Worker's name: ";
	cout << this->getName() << endl;
	cout << "Worker's seniority: ";
	cout<< seniority;
	cout << "Worker's managing seniority: ";
	cout<< manSen;
}
Worker::~Worker() // destructor for a worker.
{
}
double Worker::getVal() // a function every person has. returns average for pupil , and salary for every other worker in the school.
{
	return calcSalary();
}
bool Worker::operator >(Person& pr) // returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
{
	return getVal()>pr.getVal();
}
double Worker::calcSalary() // returns worker's salary.
{
	return 0;
}
Worker::Worker() // empty constructor for new worker.
:Person()
{
	seniority=0;
	manSen=0;
}
