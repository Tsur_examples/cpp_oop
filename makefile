# the compiler: gcc for C program, define as g++ for C++
CC = g++

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g -Wall

# the build target executable:
TARGET = Assignment6
TARGET2 = AdministrationPersonal
TARGET3 = Class
TARGET4 = Layer
TARGET5 = Manager
TARGET6 = MyArray
TARGET7 = Person
TARGET8 = Pupil
TARGET9 = School
TARGET10 = Secretary
TARGET11 = Teacher
TARGET12 = Tutor
TARGET13 = Worker
all:	clean	$(TARGET)
$(TARGET): $(TARGET).cpp
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET13).cpp $(TARGET12).cpp $(TARGET11).cpp $(TARGET10).cpp $(TARGET9).cpp $(TARGET8).cpp $(TARGET7).cpp $(TARGET6).cpp $(TARGET5).cpp $(TARGET4).cpp $(TARGET3).cpp $(TARGET2).cpp $(TARGET).cpp 

clean:
	$(RM) $(TARGET)
