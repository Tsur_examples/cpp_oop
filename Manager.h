#ifndef MANAGER_H_
#define MANAGER_H_

#include "AdministrationPersonal.h"
#include "Teacher.h"


class Manager: public AdministrationPersonal, public Teacher
{
private:
	static Manager* theMan; // static pointer to manager. NULL value at beginning.
	Manager(); //private constructor
public:
	~Manager();//destructor for manager
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	bool operator >(Person* pr);// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	static Manager* Instance();// a function that makes sure that the user cannot create more than one manager
	double calcSalary(); // returns manager's salary.
	bool isExcellent(); // returns if a specific person is outstanding (by it's type criteria)
	void printDet();// printing of tutor's full details - full name , ID, managing seniority,professions, salary, excellency.

};

#endif /* MANAGER_H_ */
