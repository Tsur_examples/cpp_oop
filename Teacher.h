#ifndef TEACHER_H_
#define TEACHER_H_
#include "Worker.h"

class Teacher:virtual public Worker
{
protected:
	vector <string> professions;
	int profsNum;
public:
	vector<string>* getProfessions();// returns reference to teacher's professions vector
	virtual double calcSalary();// returns teacher's salary.
	virtual bool isExcellent();// returns if a specific person is outstanding (by it's type criteria)
	void printProfs();// prints professions that the teacher teaches.
	virtual double getVal();// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	bool operator >(Person& pr);// returns if a person's getval value is bigger than other person's getval value. (compares salaries in worker's case)
	friend istream& operator >> (istream& input, Teacher* teach);// implementation of operator ">>" for teacher
	virtual void printDet();// printing of tutor's full details - full name , ID, seniority, salary, excellency.
	Teacher();//empty constructor for new teacher
	~Teacher();// teacher's destructor
};

#endif /* TEACHER_H_ */
