/*
 * Layer.h
 *
 *  Created on: Jun 18, 2016
 *      Author: victorr
 */

#ifndef LAYER_H_
#define LAYER_H_
#include <cstring>
#include <vector>
#include <iostream>
#include "Class.h"
using namespace std;

class Layer {
private:
	char layerName; // layer name ('a'-'f')
	vector <Class*> classes; //layer's classes vector
public:
	void addClass(Class * cl);// adds class to the layer
	Class* getClassAt(int i);// returns class at given index
	Layer(char layerName);// layer's constructor
	char getLName();// returns layer name ('a'-'f')
	~Layer();//destructor for layer
};

#endif /* LAYER_H_ */
